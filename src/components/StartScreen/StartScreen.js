import React from "react";
import {View, Text, StyleSheet} from 'react-native'

const StartScreen = () =>{
  return(
    <View style={styles.screen}>
      <Text style={styles.logo}>
        Noorhelp mobile
      </Text>
    </View>
  )
}

const styles = StyleSheet.create({
  logo:{
    fontSize: 32,
    textTransform: "uppercase"
  },
  screen:{
    display: "flex",
    alignItems: "center",
    padding: 15
  }
})

export default StartScreen
