import React from "react";
import StartScreen from "./src/components/StartScreen/StartScreen";
import { SafeAreaView } from "react-native";


const App = () => {
  return (
    <SafeAreaView>
      <StartScreen />
    </SafeAreaView>

  );
};

export default App;
